/*
 * Application.c
 *      Author: Erich Styger
 */
#include "Application.h"
#include "LEDR.h"
#include "LEDG.h"
#include "FRTOS1.h"
//#include "Shell.h"
#include "USB1.h"
#include "Tx1.h"
#include "Rx1.h"
#include "usb_cdc.h"
#include "SM1.h"

xSemaphoreHandle xUSBDataSemaphore;
xSemaphoreHandle xUSBDataDoneSemaphore;
static uint8_t cdc_buffer[USB1_DATA_BUFF_SIZE];
volatile bool OnCompleteFlg = FALSE;                                    /* Transfer complete flag */
volatile bool OnErrorFlg = FALSE;                                       /* Transfer error flag */
volatile uint16_t BytesToBeCopied;
volatile uint8_t *usbData;

static portTASK_FUNCTION(TaskUsb1, pvParameters) {
	(void)pvParameters; /* parameter not used */
	Rx1_Init();
	Tx1_Init();

	for(;;) {

		if(CDC1_App_Task(cdc_buffer, sizeof(cdc_buffer))==ERR_BUSOFF)
			LEDR_Neg();
		else
			LEDR_Off();

		//		FRTOS1_xSemaphoreGive(xSemaphore);
		FRTOS1_vTaskDelay(10/portTICK_RATE_MS);
	}
}
static portTASK_FUNCTION(TaskUSBData, pvParameters) {
	LDD_TError Error;
	(void)pvParameters; /* parameter not used */

	for(;;) {
		FRTOS1_xSemaphoreTake(xUSBDataSemaphore,portMAX_DELAY);
		LEDG_On();

		if(BytesToBeCopied>0){
			SM1_SendBlock(SM1_DeviceData,usbData,BytesToBeCopied);
			FRTOS1_xSemaphoreTake(xUSBDataDoneSemaphore,portMAX_DELAY);
		}

		(void)USB_Class_CDC_Interface_DIC_Recv_Data(0, NULL, 0); /* see http://eprints.utar.edu.my/143/1/BI-2011-0708672-1.pdf, page 131 */
		LEDG_Off();

	}
}

void APP_Run(void) {
	//  SHELL_Init();
	if (FRTOS1_xTaskCreate(
			TaskUsb1,  /* pointer to the task */
			(signed portCHAR *)"Task USB status", /* task name for kernel awareness debugging */
			configMINIMAL_STACK_SIZE, /* task stack size */
			(void*)NULL, /* optional task startup argument */
			tskIDLE_PRIORITY,  /* initial priority */
			(xTaskHandle*)NULL /* optional task handle to create */
	) != pdPASS) {
		/*lint -e527 */
		for(;;){}; /* error! probably out of memory */
		/*lint +e527 */
	}
	if (FRTOS1_xTaskCreate(
			TaskUSBData,  /* pointer to the task */
			(signed portCHAR *)"Task USB Data", /* task name for kernel awareness debugging */
			configMINIMAL_STACK_SIZE, /* task stack size */
			(void*)NULL, /* optional task startup argument */
			tskIDLE_PRIORITY,  /* initial priority */
			(xTaskHandle*)NULL /* optional task handle to create */
	) != pdPASS) {
		/*lint -e527 */
		for(;;){}; /* error! probably out of memory */
		/*lint +e527 */
	}
	xUSBDataSemaphore = xSemaphoreCreateCounting(1,0);
	xUSBDataDoneSemaphore = xSemaphoreCreateCounting(1,0);

	FRTOS1_vTaskStartScheduler();
}

